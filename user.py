import znc

class user(znc.Module):
    module_types = [znc.CModInfo.GlobalModule]
    description = "A python implementation to add users to the znc database" 
    wiki_page = "user"
    
    def onLoad(self, args, message):
        self.PutModule("WooHoo I'm loaded")
    def onShutdown(self, args, message):
        self.PutModule("Unloading...")
    def onModCommand(self, command):
        cmds = """help, adduser"""
        znc.PutModule(command)
        if command[0].lower().startswith("adduser"):
            username = command[1]
            self.PutModule("Adding user %s.")
            new_user = znc.CUser(username)
            str_err = znc.String()
            if znc.CZNC.Get().AddUser(new_user, str_err):
                new_user.thisown = 0
